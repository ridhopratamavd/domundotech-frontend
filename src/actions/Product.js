import * as api from '../product/Api';

export const getProducts = () => async (dispatch) => {
  try {
    dispatch({ type: 'IN_PROGRESS' });
    const { data } = await api.getProducts().catch(() => {
      setTimeout(() => {
        const progressAction = { type: 'IN_PROGRESS_DONE' };
        dispatch(progressAction);
        const notificationAction = { type: 'NOTIFICATION_TIMEOUT', message: 'Backend down', notificationType: 'error' };
        dispatch(notificationAction);
      }, 3000);
    });

    const action = { type: 'FETCH_ALL_PRODUCTS', payload: data };
    dispatch(action);
    const progressAction = { type: 'IN_PROGRESS_DONE' };
    dispatch(progressAction);
  } catch (error) {
    console.log(error);
  }
};

export const createProduct = (product) => async (dispatch) => {
  try {
    await api.createProduct(product).then(({ data }) => {
      if (data.status.code === '00' && data.status.description === 'Success') {
        const action = { type: 'CREATE_PRODUCTS', payload: data.result[0] };
        dispatch(action);
        const notificationAction = {
          type: 'NOTIFICATION_SUCCESS',
          message: 'Success create new record',
          notificationType: 'success'
        };
        dispatch(notificationAction);
        const progressAction = { type: 'IN_PROGRESS_DONE' };
        dispatch(progressAction);
      } else if (data.status.code === '01' && data.status.description === 'Already exist') {
        const action = { type: 'CREATE', payload: data.result[0] };
        dispatch(action);
        const notificationAction = {
          type: 'NOTIFICATION_SUCCESS',
          message: 'Record already exist',
          notificationType: 'warning'
        };
        dispatch(notificationAction);
        const progressAction = { type: 'IN_PROGRESS_DONE' };
        dispatch(progressAction);
      } else {
        console.log(data);
      }
    }).catch(() => {
      setTimeout(() => {
        const progressAction = { type: 'IN_PROGRESS_DONE' };
        dispatch(progressAction);
        const notificationAction = { type: 'NOTIFICATION_TIMEOUT', message: 'Backend down', notificationType: 'error' };
        dispatch(notificationAction);
      }, 1000);
    });
    dispatch(getProducts());
  } catch (error) {
    console.log(error);
  }
};

export const createSoldItem = (soldItem) => async (dispatch) => {
  try {
    await api.createSoldItem(soldItem).then(({ data }) => {
      if (data.status.code === '00' && data.status.description === 'Success') {
        const action = { type: 'CREATE_SOLD_ITEM', payload: data.result[0] };
        dispatch(action);
        const notificationAction = {
          type: 'NOTIFICATION_SUCCESS',
          message: 'Success update stock',
          notificationType: 'success'
        };
        dispatch(notificationAction);
        const progressAction = { type: 'IN_PROGRESS_DONE' };
        dispatch(progressAction);
      } else {
        setTimeout(() => {
          const progressAction = { type: 'IN_PROGRESS_DONE' };
          dispatch(progressAction);
          const notificationAction = { type: 'NOTIFICATION_TIMEOUT', message: 'Failed to decrement stock', notificationType: 'error' };
          dispatch(notificationAction);
        }, 1000);
      }
    }).catch(() => {
      setTimeout(() => {
        const progressAction = { type: 'IN_PROGRESS_DONE' };
        dispatch(progressAction);
        const notificationAction = { type: 'NOTIFICATION_TIMEOUT', message: 'Backend down', notificationType: 'error' };
        dispatch(notificationAction);
      }, 1000);
    });
    dispatch(getProducts());
  } catch (error) {
    console.log(error);
  }
};
