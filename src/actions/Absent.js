import * as api from '../absent/Api';

export const getAbsents = () => async (dispatch) => {
  try {
    dispatch({ type: 'IN_PROGRESS' });
    const { data } = await api.getAbsents().catch(() => {
      setTimeout(() => {
        const progressAction = { type: 'IN_PROGRESS_DONE' };
        dispatch(progressAction);
        const notificationAction = { type: 'NOTIFICATION_TIMEOUT', message: 'Backend down', notificationType: 'error' };
        dispatch(notificationAction);
      }, 3000);
    });

    const action = { type: 'FETCH_ALL', payload: data };
    dispatch(action);
    const progressAction = { type: 'IN_PROGRESS_DONE' };
    dispatch(progressAction);
  } catch (error) {
    console.log(error);
  }
};

export const createAbsent = (absent) => async (dispatch) => {
  try {
    await api.createAbsent(absent).then(({ data }) => {
      if (data.status.code === '00' && data.status.description === 'Success') {
        const action = { type: 'CREATE', payload: data.result[0] };
        dispatch(action);
        const notificationAction = { type: 'NOTIFICATION_SUCCESS', message: 'Success create new record', notificationType: 'success' };
        dispatch(notificationAction);
        const progressAction = { type: 'IN_PROGRESS_DONE' };
        dispatch(progressAction);
      } else if (data.status.code === '01' && data.status.description === 'Already exist') {
        const action = { type: 'CREATE', payload: data.result[0] };
        dispatch(action);
        const notificationAction = { type: 'NOTIFICATION_SUCCESS', message: 'Record already exist', notificationType: 'warning' };
        dispatch(notificationAction);
        const progressAction = { type: 'IN_PROGRESS_DONE' };
        dispatch(progressAction);
      } else {
        console.log(data);
      }
    }).catch(() => {
      setTimeout(() => {
        const progressAction = { type: 'IN_PROGRESS_DONE' };
        dispatch(progressAction);
        const notificationAction = { type: 'NOTIFICATION_TIMEOUT', message: 'Backend down', notificationType: 'error' };
        dispatch(notificationAction);
      }, 1000);
    });
    dispatch(getAbsents());
  } catch (error) {
    console.log(error);
  }
};
