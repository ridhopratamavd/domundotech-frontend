import React, { useEffect } from 'react';
import { makeStyles, createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import { Box, Card } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { getProducts } from '../actions/Product';
import MUIDataTable from 'mui-datatables';
import NumberFormat from 'react-number-format';

const columns = [
  {
    name: 'name',
    label: 'Name',
    options: {
      filter: true,
      sort: true
    }
  },
  {
    name: 'buyPrice',
    label: 'Buy Price',
    options: {
      sort: true,
      customBodyRender: (value, tableMeta, updateValue) => (
        <NumberFormat
          displayType={'text'}
          value={value}
          thousandSeparator={true}
          isNumericString={true}
          prefix={'Rp. '}
        />
      )
    }
  },
  {
    name: 'offlineSellPrice',
    label: 'Offline Price',
    options: {
      sort: true,
      customBodyRender: (value, tableMeta, updateValue) => (
        <NumberFormat
          displayType={'text'}
          value={value}
          thousandSeparator={true}
          isNumericString={true}
          prefix={'Rp. '}
        />
      )
    }
  },
  {
    name: 'onlineSellPrice',
    label: 'Online Price',
    options: {
      sort: true,
      customBodyRender: (value, tableMeta, updateValue) => (
        <NumberFormat
          displayType={'text'}
          value={value}
          thousandSeparator={true}
          isNumericString={true}
          prefix={'Rp. '}
        />
      )
    }
  },
  {
    name: 'productQuantity',
    label: 'Quantity',
    options: {
      sort: true
    }
  }
];

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    marginTop: '30px',
    padding: '0px 15px 0px 15px',
    marginBottom: '40px'
  },
  card: {
    padding: '30px 30px 30px 30px',
    minWidth: '500px'
  }
}));

export default function Report () {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getProducts());
  }, [dispatch]);

  const dataForTable = useSelector(state => {
    return state.Products;
  });

  const classes = useStyles();

  const getMuiTheme = () =>
    createMuiTheme({
      overrides: {
        MUIDataTable: {
          paper: {
            boxShadow: 'none'
          }
        }
      }
    });

  return (
    <Box justifyContent='flex-end' className={classes.root}>
      <Card className={classes.card} variant='outlined'>
        <MuiThemeProvider theme={getMuiTheme()}>
          <MUIDataTable
            title={'Product List'}
            data={dataForTable}
            columns={columns}
            options={{
              selectableRows: 'none' // <===== will turn off checkboxes in rows
            }}
          />
        </MuiThemeProvider>
      </Card>
    </Box>
  );
}
