import React, { useState, useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import { Box, FormGroup, Card, Button, Typography, TextField } from '@material-ui/core';
import { Autocomplete } from '@material-ui/lab';
import Notification from '../universalComponents/Notification';
import Progress from '../universalComponents/Progress';
import { makeStyles } from '@material-ui/core/styles';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import { getProductReferences } from '../actions/Reference';
import NumberFormat from 'react-number-format';
import { createProduct } from '../actions/Product';
import { PriceInput } from '../utils/PriceInput';

function priceInput (props) {
  const { inputRef, onChange, ...other } = props;
  return (
    <NumberFormat
      {...other}
      getInputRef={inputRef}
      onValueChange={(values) => {
        onChange({
          target: {
            value: values.value
          }
        });
      }}
      thousandSeparator
      isNumericString
      prefix='Rp. '
    />
  );
}

priceInput.propTypes = {
  inputRef: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired
};

function productQuantityInput (props) {
  const { inputRef, onChange, ...other } = props;
  return (
    <NumberFormat
      {...other}
      getInputRef={inputRef}
      onValueChange={(values) => {
        onChange({
          target: {
            value: values.value
          }
        });
      }}
      isNumericString
    />
  );
}

productQuantityInput.propTypes = {
  inputRef: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired
};

export default function ProductAdd () {
  const [name, setName] = useState('');
  const [productType, setProductType] = useState({ id: null, name: '' });
  const [buyPrice, setBuyPrice] = useState('');
  const [offlineSellPrice, setOfflineSellPrice] = useState('');
  const [onlineSellPrice, setOnlineSellPrice] = useState('');
  const [productQuantity, setProductQuantity] = useState('');
  const [description, setDescription] = useState('');
  const [notes, setNotes] = useState('');
  const [tokoPointIn, setTokoPointIn] = useState('');
  const [tokoPointOut, setTokoPointOut] = useState('');
  const [ovoIn, setOvoIn] = useState('');
  const [ovoOut, setOvoOut] = useState('');
  const [bcaIn, setBcaIn] = useState('');
  const [bcaOut, setBcaOut] = useState('');
  const [cashIn, setCashIn] = useState('');
  const [cashOut, setCashOut] = useState('');

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getProductReferences());
  }, [dispatch]);

  const references = useSelector(state => {
    return [{ id: null, name: '' }, ...state.Reference];
  });

  const useStyles = makeStyles({
    root: {
      marginTop: '30px',
      padding: '0px 15px 0px 15px '
    },
    button: {
      background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
      border: 0,
      borderRadius: 3,
      boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
      color: 'white',
      height: 48,
      padding: '0 30px'
    },
    card: {
      padding: '30px 30px 30px 30px',
      minWidth: '500px'
    },
    input: {
      marginTop: '20px'
    },
    fullWidth: {
      width: '100%'
    }
  });

  const productFormStyles = useStyles();
  const referenceForms = useRef();

  const clearForm = () => {
    referenceForms.current.resetValidations();
    setName('');
    setProductType({ id: null, name: '' });
    setBuyPrice('');
    setOfflineSellPrice('');
    setOnlineSellPrice('');
    setDescription('');
    setNotes('');
    setProductQuantity('');
    setTokoPointIn('');
    setTokoPointOut('');
    setOvoIn('');
    setOvoOut('');
    setBcaIn('');
    setBcaOut('');
    setCashIn('');
    setCashOut('');
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const create = createProduct({
      name: name,
      buyPrice: buyPrice,
      offlineSellPrice: offlineSellPrice,
      onlineSellPrice: onlineSellPrice,
      productType: productType.id,
      productQuantity: productQuantity,
      description: description,
      notes: notes,
      tokoPointIn: tokoPointIn !== '' ? tokoPointIn : null,
      tokoPointOut: tokoPointOut !== '' ? tokoPointOut : null,
      bcaIn: bcaIn !== '' ? bcaIn : null,
      bcaOut: bcaOut !== '' ? bcaOut : null,
      ovoIn: ovoIn !== '' ? ovoIn : null,
      ovoOut: ovoOut !== '' ? ovoOut : null,
      cashIn: cashIn !== '' ? cashIn : null,
      cashOut: cashOut !== '' ? cashOut : null
    });
    dispatch(create);
    dispatch({ type: 'IN_PROGRESS' });
    clearForm();
  };

  return (
    <>
      <Progress />
      <Box justifyContent='flex-start' className={productFormStyles.root}>
        <Card className={productFormStyles.card} variant='outlined'>
          <Typography variant='h2'>Add</Typography>
          <Typography variant='h6'>cek dulu ada gk barang existing, harga belanja nya beda gk</Typography>
          <ValidatorForm
            ref={referenceForms}
            component='form'
            onSubmit={handleSubmit}
          >
            <FormGroup>
              <Autocomplete
                className={[productFormStyles.input, productFormStyles.fullWidth].join(' ')}
                options={references}
                getOptionLabel={(option) => {
                  return option.name;
                }}
                getOptionSelected={(option, value) => option.id === value.id}
                value={productType}
                onChange={(_, value) => {
                  setProductType({ id: value?.id, name: value?.name });
                }}
                renderInput={(params) => <TextValidator {...params} label='Product type*'
                  value={productType.name}
                  validators={['required']}
                  errorMessages={['please insert name']}/>}
              />
              <TextValidator
                label='Product name*'
                onChange={(e) => setName(e.target.value)}
                value={name}
                validators={['required']}
                errorMessages={['please insert name']}
                className={[productFormStyles.input, productFormStyles.fullWidth].join(' ')}
                helperText={'Totolink WN725, Acer charger 3.42A 19V 5 x 1.56, Corsair DDR3L 4GB 1600MHz'}
              >
              </TextValidator>
              <TextValidator
                label='Buy price*'
                className={[productFormStyles.input, productFormStyles.fullWidth].join(' ')}
                onChange={(event) => setBuyPrice(event.target.value)}
                value={buyPrice}
                validators={['required']}
                errorMessages={['required']}
                InputProps={{
                  inputComponent: PriceInput
                }}
              />
              <Box display='flex' flexWrap="nowrap" justifyContent="space-between">
                <TextValidator
                  label='Offline Sell price*'
                  className={productFormStyles.input}
                  onChange={(event) => setOfflineSellPrice(event.target.value)}
                  value={offlineSellPrice}
                  validators={['required', 'minNumber:0']}
                  errorMessages={['required', 'minimum 0']}
                  InputProps={{
                    inputComponent: PriceInput
                  }}
                />
                <TextValidator
                  label='Online Sell price*'
                  className={productFormStyles.input}
                  onChange={(event) => setOnlineSellPrice(event.target.value)}
                  value={onlineSellPrice}
                  validators={['required', 'minNumber:0']}
                  errorMessages={['required', 'minimum 0']}
                  InputProps={{
                    inputComponent: PriceInput
                  }}
                />
              </Box>
              <Box display='flex' flexWrap="nowrap" justifyContent="space-between">
                <TextValidator
                  label='TokoPoint masuk'
                  className={productFormStyles.input}
                  onChange={(event) => setTokoPointIn(event.target.value)}
                  value={tokoPointIn}
                  validators={['minNumber:0']}
                  errorMessages={['minimum 0']}
                  InputProps={{
                    inputComponent: PriceInput
                  }}
                />
                <TextValidator
                  label='TokoPoint keluar'
                  className={productFormStyles.input}
                  onChange={(event) => setTokoPointOut(event.target.value)}
                  value={tokoPointOut}
                  validators={['minNumber:0']}
                  errorMessages={['minimum 0']}
                  InputProps={{
                    inputComponent: PriceInput
                  }}
                />
              </Box>
              <Box display='flex' flexWrap="nowrap" justifyContent="space-between">
                <TextValidator
                  label='Ovo masuk'
                  className={productFormStyles.input}
                  onChange={(event) => setOvoIn(event.target.value)}
                  value={ovoIn}
                  validators={['minNumber:0']}
                  errorMessages={['minimum 0']}
                  InputProps={{
                    inputComponent: PriceInput
                  }}
                />
                <TextValidator
                  label='Ovo keluar'
                  className={productFormStyles.input}
                  onChange={(event) => setOvoOut(event.target.value)}
                  value={ovoOut}
                  validators={['minNumber:0']}
                  errorMessages={['minimum 0']}
                  InputProps={{
                    inputComponent: PriceInput
                  }}
                />
              </Box>

              <Box display='flex' flexWrap="nowrap" justifyContent="space-between">
                <TextValidator
                  label='Bca masuk'
                  className={productFormStyles.input}
                  onChange={(event) => setBcaIn(event.target.value)}
                  value={bcaIn}
                  validators={['minNumber:0']}
                  errorMessages={['minimum 0']}
                  InputProps={{
                    inputComponent: PriceInput
                  }}
                />
                <TextValidator
                  label='Bca keluar'
                  className={productFormStyles.input}
                  onChange={(event) => setBcaOut(event.target.value)}
                  value={bcaOut}
                  validators={['minNumber:0']}
                  errorMessages={['minimum 0']}
                  InputProps={{
                    inputComponent: PriceInput
                  }}
                />
              </Box>

              <Box display='flex' flexWrap="nowrap" justifyContent="space-between">
                <TextValidator
                  label='Cash masuk'
                  className={productFormStyles.input}
                  onChange={(event) => setCashIn(event.target.value)}
                  value={cashIn}
                  validators={['minNumber:0']}
                  errorMessages={['minimum 0']}
                  InputProps={{
                    inputComponent: PriceInput
                  }}
                />
                <TextValidator
                  label='Cash keluar'
                  className={productFormStyles.input}
                  onChange={(event) => setCashOut(event.target.value)}
                  value={cashOut}
                  validators={['minNumber:0']}
                  errorMessages={['minimum 0']}
                  InputProps={{
                    inputComponent: PriceInput
                  }}
                />
              </Box>

              <TextValidator
                label='Product Quantity*'
                className={[productFormStyles.input, productFormStyles.fullWidth].join(' ')}
                onChange={(event) => setProductQuantity(event.target.value)}
                value={productQuantity}
                validators={['required', 'minNumber:1']}
                errorMessages={['required', 'minimum 1']}
                InputProps={{
                  inputComponent: productQuantityInput
                }}
              />
              <TextField
                className={productFormStyles.input}
                label='Description'
                multiline
                value={description}
                onChange={(event) => setDescription(event.target.value)}
              />
              <TextField
                className={productFormStyles.input}
                label='Notes'
                multiline
                value={notes}
                onChange={(event) => setNotes(event.target.value)}
              />
              <br></br>
              <Button className={productFormStyles.button} color='primary' type='submit'>Submit</Button>
              <Button variant='contained' onClick={ () => clearForm() } >Reset form</Button>
            </FormGroup>
          </ValidatorForm>
        </Card>
      </Box>
      <Notification/>
    </>
  );
}
