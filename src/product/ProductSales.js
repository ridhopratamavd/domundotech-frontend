/* eslint-disable */
import React, { useRef, useState } from 'react';
import { Box, Button, Card, FormGroup, makeStyles, TextField, Typography, Checkbox, FormControlLabel, Divider } from '@material-ui/core';
import { Autocomplete } from '@material-ui/lab';
import { TextValidator, ValidatorForm } from 'react-material-ui-form-validator';
import { useDispatch, useSelector } from 'react-redux';
import { createSoldItem } from '../actions/Product';
import NumberFormat from 'react-number-format';
import PropTypes from 'prop-types';
import { PriceInput } from '../utils/PriceInput';

import { ShoppingCart } from '@material-ui/icons/';
export default function ProductSales () {
  const [selectedProduct, setSelectedProduct] = useState({ id: null, name: '' });
  const [invoiceNumber, setInvoiceNumber] = useState('');
  const [note, setNote] = useState('');
  const [soldQuantity, setSoldQuantity] = useState('');
  const [isFromOfflineStore, setIsFromOffline] = React.useState(true);
  const [tokoPointIn, setTokoPointIn] = useState('');
  const [tokoPointOut, setTokoPointOut] = useState('');
  const [ovoIn, setOvoIn] = useState('');
  const [ovoOut, setOvoOut] = useState('');
  const [bcaIn, setBcaIn] = useState('');
  const [bcaOut, setBcaOut] = useState('');
  const [cashIn, setCashIn] = useState('');
  const [cashOut, setCashOut] = useState('');
  const [onlineSellPrice, setOnlineSellPrice] = useState('');
  const [offlineSellPrice, setOfflineSellPrice] = useState('');
  const [cart, setCart] = useState([]);
  const [availableQuantity, setAvailableQuantity] = useState('');
  const [totalBill, setTotalBill] = useState(0);

  const useStyles = makeStyles({
    root: {
      marginTop: '30px',
      padding: '0px 15px 0px 15px '
    },
    button: {
      background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
      border: 0,
      borderRadius: 3,
      boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
      color: 'white',
      height: 48,
      padding: '0 30px',
    },
    card: {
      padding: '30px 30px 30px 30px',
      minWidth: '500px',
    },
    input: {
      width: '100%',
      marginTop: '20px'
    },
    salesForm: {
      display: 'flex'
    }
  });

  const dispatch = useDispatch();
  const products = useSelector(state => {
    if (state.Products.length !== 0) {
      return [{ id: null, name: '' }, ...state.Products.filter(availableProduct => availableProduct.productQuantity !== 0)];
    }
    return [];
  });

  const productFormStyles = useStyles();
  const referenceForms = useRef();

  function soldQuantityInput (props) {
    const { inputRef, onChange, ...other } = props;
    return (
      <NumberFormat
        {...other}
        getInputRef={inputRef}
        onValueChange={(values) => {
          onChange({
            target: {
              value: values.value
            }
          });
        }}
        isNumericString
      />
    );
  }

  soldQuantityInput.propTypes = {
    inputRef: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired
  };

  const clearStockForm = () => {
    referenceForms.current.resetValidations();
    setSelectedProduct({ id: null, name: '' });
    setSoldQuantity('');
    setTokoPointIn('');
    setTokoPointOut('');
    setOvoIn('');
    setOvoOut('');
    setBcaIn('');
    setBcaOut('');
    setCashIn('');
    setCashOut('');
    setOnlineSellPrice('');
    setOfflineSellPrice('');
    setAvailableQuantity('');
  };

  const clearForm = () => {
    clearStockForm();
    setInvoiceNumber('');
    setNote('');
    setIsFromOffline(true);
    setTotalBill(0);
    setCart([]);
  };

  const handleSubmitCart = (e) => {
    e.preventDefault();
    const create = createSoldItem({
      selectedProduct: selectedProduct.id,
      soldQuantity,
      invoiceNumber,
      note,
      isFromOfflineStore,
      tokoPointIn: tokoPointIn !== '' ? tokoPointIn : null,
      tokoPointOut: tokoPointOut !== '' ? tokoPointOut : null,
      bcaIn: bcaIn !== '' ? bcaIn : null,
      bcaOut: bcaOut !== '' ? bcaOut : null,
      ovoIn: ovoIn !== '' ? ovoIn : null,
      ovoOut: ovoOut !== '' ? ovoOut : null,
      cashIn: cashIn !== '' ? cashIn : null,
      cashOut: cashOut !== '' ? cashOut : null
    });
    dispatch(create);
    dispatch({ type: 'IN_PROGRESS' });
    clearForm();
  };

  const handleSubmitItemSold = (e) => {
    e.preventDefault();
    let priceAmount = parseInt(isFromOfflineStore ? offlineSellPrice : onlineSellPrice);
    priceAmount *= parseInt(soldQuantity);
    setTotalBill(totalBill + priceAmount);
    setCart([...cart, { selectedProduct: selectedProduct, soldQuantity, priceAmount }]);
    clearStockForm();
  };

  const handleChange = (event) => {
    setIsFromOffline(event.target.checked);
  };

  return (
    <div>
      <Box className={productFormStyles.root}>
        <Card className={productFormStyles.card} variant='outlined'>
          <Typography variant='h2'>Sales</Typography>
          <Typography variant='h6'>form untuk mengurangi stok barang yang laku terjual</Typography>
          <ValidatorForm
            ref={referenceForms}
            component='form'
            onSubmit={handleSubmitItemSold}
          >
            <Box className={productFormStyles.salesForm}>
              <Box style={{ width: '420px', paddingRight: '50px', marginRight: '50px', borderRight: '1px solid black' }}>
                <FormGroup>
                  <Autocomplete
                    className={productFormStyles.input}
                    options={products}
                    value={selectedProduct}
                    getOptionLabel={(option) => {
                      return option.name;
                    }}
                    getOptionSelected={(option, value) => option.id === value.id}
                    onChange={(_, value) => {
                      setSelectedProduct({ id: value?.id, name: value?.name });
                      setOnlineSellPrice(value?.onlineSellPrice);
                      setOfflineSellPrice(value?.offlineSellPrice);
                      setAvailableQuantity(value?.productQuantity);
                      if (value === null) {
                        setOnlineSellPrice('');
                        setOfflineSellPrice('');
                        setAvailableQuantity('');
                      }
                    }}
                    renderInput={(params) => <TextValidator {...params} label='Product*'
                      value={selectedProduct?.name}
                      validators={['required']}
                      errorMessages={['please insert product']}/>}
                  />
                  <Box display='flex' flexWrap="nowrap" justifyContent="space-between">
                    <TextValidator
                      label='Online price'
                      className={productFormStyles.input}
                      value={onlineSellPrice}
                      InputProps={{
                        inputComponent: PriceInput
                      }}
                      disabled
                    />
                    <TextValidator
                      label='Offline price'
                      className={productFormStyles.input}
                      value={offlineSellPrice}
                      InputProps={{
                        inputComponent: PriceInput
                      }}
                      disabled
                    />
                  </Box>
                  <Box display='flex' flexWrap="nowrap" justifyContent="space-between">
                    <TextValidator
                      label='Available Quantity*'
                      className={productFormStyles.input}
                      value={availableQuantity}
                      disabled
                    />
                    <TextValidator
                      label='Sold Quantity*'
                      className={productFormStyles.input}
                      onChange={(event) => setSoldQuantity(event.target.value)}
                      value={soldQuantity}
                      validators={['required', `maxNumber:${availableQuantity}`, 'minNumber:1']}
                      errorMessages={['required', `maximum ${availableQuantity}`, 'minNumber 1']}
                    />
                  </Box>
                  <Box display='flex' flexWrap="nowrap" justifyContent="space-between">
                    <TextValidator
                      label='TokoPoint masuk'
                      className={productFormStyles.input}
                      onChange={(event) => setTokoPointIn(event.target.value)}
                      value={tokoPointIn}
                      validators={['minNumber:0']}
                      errorMessages={['minimum 0']}
                      InputProps={{
                        inputComponent: PriceInput
                      }}
                    />
                    <TextValidator
                      label='TokoPoint keluar'
                      className={productFormStyles.input}
                      onChange={(event) => setTokoPointOut(event.target.value)}
                      value={tokoPointOut}
                      validators={['minNumber:0']}
                      errorMessages={['minimum 0']}
                      InputProps={{
                        inputComponent: PriceInput
                      }}
                    />
                  </Box>
                  <Box display='flex' flexWrap="nowrap" justifyContent="space-between">
                    <TextValidator
                      label='Ovo masuk'
                      className={productFormStyles.input}
                      onChange={(event) => setOvoIn(event.target.value)}
                      value={ovoIn}
                      validators={['minNumber:0']}
                      errorMessages={['minimum 0']}
                      InputProps={{
                        inputComponent: PriceInput
                      }}
                    />
                    <TextValidator
                      label='Ovo keluar'
                      className={productFormStyles.input}
                      onChange={(event) => setOvoOut(event.target.value)}
                      value={ovoOut}
                      validators={['minNumber:0']}
                      errorMessages={['minimum 0']}
                      InputProps={{
                        inputComponent: PriceInput
                      }}
                    />
                  </Box>
                  <Box display='flex' flexWrap="nowrap" justifyContent="space-between">
                    <TextValidator
                      label='Bca masuk'
                      className={productFormStyles.input}
                      onChange={(event) => setBcaIn(event.target.value)}
                      value={bcaIn}
                      validators={['minNumber:0']}
                      errorMessages={['minimum 0']}
                      InputProps={{
                        inputComponent: PriceInput
                      }}
                    />
                    <TextValidator
                      label='Bca keluar'
                      className={productFormStyles.input}
                      onChange={(event) => setBcaOut(event.target.value)}
                      value={bcaOut}
                      validators={['minNumber:0']}
                      errorMessages={['minimum 0']}
                      InputProps={{
                        inputComponent: PriceInput
                      }}
                    />
                  </Box>

                  <Box display='flex' flexWrap="nowrap" justifyContent="space-between">
                    <TextValidator
                      label='Cash masuk'
                      className={productFormStyles.input}
                      onChange={(event) => setCashIn(event.target.value)}
                      value={cashIn}
                      validators={['minNumber:0']}
                      errorMessages={['minimum 0']}
                      InputProps={{
                        inputComponent: PriceInput
                      }}
                    />
                    <TextValidator
                      label='Cash keluar'
                      className={productFormStyles.input}
                      onChange={(event) => setCashOut(event.target.value)}
                      value={cashOut}
                      validators={['minNumber:0']}
                      errorMessages={['minimum 0']}
                      InputProps={{
                        inputComponent: PriceInput
                      }}
                    />
                  </Box>

                  <Button className={productFormStyles.button} color='primary' type='submit' ><ShoppingCart/> Add to Cart</Button>

                  <TextField
                    label='Nomor Nota'
                    value={invoiceNumber}
                    onChange={(event) => setInvoiceNumber(event.target.value)}
                  />
                  <FormControlLabel
                    control={<Checkbox checked={isFromOfflineStore} onChange={handleChange} name="checkedA" />}
                    label="Is from offline store?"
                  />
                  <TextField
                    label='Catatan'
                    multiline
                    value={note}
                    onChange={(event) => setNote(event.target.value)}
                  />
                </FormGroup>
              </Box>
              <Box style={{ width: '500px' }}>
                <Typography variant='h4'>Cart</Typography>
                <Box >
                  {cart.map(item => <Box style={{ display: 'flex', justifyContent: 'space-between' }}>
                    <p>{ `${item.soldQuantity} x ${item.selectedProduct.name}`}</p>
                    <p>
                      { `${new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR' }).format(item.priceAmount)}`}</p>
                  </Box>)}
                </Box>
                <Box style={{ display: 'flex', justifyContent: 'flex-end', alignItems: 'flex-end' }}>
                  <Typography variant='h6'>Total bill : { `${new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR' }).format(totalBill)}`}</Typography>
                </Box>
              </Box>
            </Box>
            <Box className={productFormStyles.salesForm} style={{ justifyContent: 'flex-end' }}>
              <br/>
              <Button className={productFormStyles.button} color='primary'>Submit</Button>
              <Button variant='contained' onClick={ () => clearForm() } >Reset form</Button>
            </Box>
          </ValidatorForm>
        </Card>
        <Card>

        </Card>
      </Box>
    </div>
  );
}
