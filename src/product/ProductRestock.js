import React, { useRef, useState } from 'react';
import { Box, Button, Card, FormGroup, makeStyles, TextField, Typography } from '@material-ui/core';
import { Autocomplete } from '@material-ui/lab';
import { TextValidator, ValidatorForm } from 'react-material-ui-form-validator';
import { useDispatch, useSelector } from 'react-redux';
import { createSoldItem } from '../actions/Product';
import NumberFormat from 'react-number-format';
import PropTypes from 'prop-types';

export default function ProductRestock () {
  const [selectedProduct, setSelectedProduct] = useState({ id: null, name: '' });
  const [note, setNote] = useState('');
  const [buyQuantity, setBuyQuantity] = useState(null);
  const useStyles = makeStyles({
    root: {
      marginTop: '30px',
      padding: '0px 15px 0px 15px '
    },
    button: {
      background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
      border: 0,
      borderRadius: 3,
      boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
      color: 'white',
      height: 48,
      padding: '0 30px'
    },
    card: {
      padding: '30px 30px 30px 30px',
      minWidth: '500px'
    },
    input: {
      width: '100%',
      marginTop: '20px'
    }
  });

  const dispatch = useDispatch();
  const products = useSelector(state => {
    if (state.Products.length !== 0) {
      return [{ id: null, name: '' }, ...state.Products.filter(availableProduct => availableProduct.productQuantity !== 0)];
    }
    return [];
  });

  const productFormStyles = useStyles();
  const referenceForms = useRef();

  function soldQuantityInput (props) {
    const { inputRef, onChange, ...other } = props;
    return (
      <NumberFormat
        {...other}
        getInputRef={inputRef}
        onValueChange={(values) => {
          onChange({
            target: {
              value: values.value
            }
          });
        }}
        isNumericString
      />
    );
  }

  soldQuantityInput.propTypes = {
    inputRef: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired
  };

  const clearForm = () => {
    referenceForms.current.resetValidations();
    setSelectedProduct(null);
    setNote('');
    setBuyQuantity('');
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const create = createSoldItem({
      selectedProduct: selectedProduct.id,
      buyQuantity,
      note
    });
    dispatch(create);
    dispatch({ type: 'IN_PROGRESS' });
    clearForm();
  };

  return (
    <div>
      <Box justifyContent='flex-start' className={productFormStyles.root}>
        <Card className={productFormStyles.card} variant='outlined'>
          <Typography variant='h4'>Restock</Typography>
          <Typography variant='h6'>pastikan harga beli sama. kalau beda, mending add product baru</Typography>
          <ValidatorForm
            ref={referenceForms}
            component='form'
            onSubmit={handleSubmit}
          >
            <FormGroup>
              <Autocomplete
                className={productFormStyles.input}
                options={products}
                value={selectedProduct}
                getOptionLabel={(option) => {
                  return option.name;
                }}
                getOptionSelected={(option, value) => option.id === value.id}
                onChange={(_, value) => {
                  setSelectedProduct({ id: value?.id, name: value?.name });
                }}
                renderInput={(params) => <TextValidator {...params} label='Product*'
                  value={selectedProduct?.name}
                  validators={['required']}
                  errorMessages={['please insert product']}/>}
              />
              <TextValidator
                label='Buy Quantity*'
                className={productFormStyles.input}
                onChange={(event) => setBuyQuantity(event.target.value)}
                value={buyQuantity}
                validators={['required', 'minNumber:1']}
                errorMessages={['required', 'minimum 1']}
              />
              <TextField
                label='Catatan'
                multiline
                value={note}
                onChange={(event) => setNote(event.target.value)}
              />
              <br></br>
              <Button className={productFormStyles.button} color='primary' type='submit'>Submit</Button>
              <Button variant='contained' onClick={ () => clearForm() } >Reset form</Button>
            </FormGroup>
          </ValidatorForm>
        </Card>
      </Box>
    </div>
  );
}
