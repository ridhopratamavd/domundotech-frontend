import axios from 'axios';

const PRODUCTS_URL = 'http://localhost:5000/product';

export const getProducts = () => axios.get(PRODUCTS_URL, { timeout: 5000 });
export const createProduct = (newProduct) => axios.post(PRODUCTS_URL, newProduct, { timeout: 5000 });
export const createSoldItem = (soldItem) => axios.post(PRODUCTS_URL.concat('/sold-item'), soldItem, { timeout: 5000 });
