import React from 'react';
import ProductAdd from './ProductAdd';
import Report from './Report';
import Box from '@material-ui/core/Box';
import { makeStyles } from '@material-ui/core';
import ProductSales from './ProductSales';

export default function index () {
  const useStyles = makeStyles({
    box: {
      padding: '15px 15px 15px 15px ',
      width: '100%'
    }
  });

  const indexProductStyles = useStyles();

  return (
    <>
      <Box display="flex" flexWrap="wrap" alignContent="space-around" className={indexProductStyles.box}>
        <ProductSales/>
        <ProductAdd/>
        <Report/>
      </Box>
    </>
  );
}
