import axios from 'axios';

const REFERENCE_URL = 'http://localhost:5000/reference';

export const getReferences = () => axios.get(REFERENCE_URL, { timeout: 5000 });
export const getProductReferences = () => axios.get(REFERENCE_URL.concat('?reference_type=PRODUCT_TYPE'), { timeout: 5000 });
export const createReference = (newReference) => axios.post(REFERENCE_URL, newReference, { timeout: 5000 });
