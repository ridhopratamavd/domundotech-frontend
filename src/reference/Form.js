import React, { useState, useRef } from 'react';
import { useDispatch } from 'react-redux';
import { createReference } from '../actions/Reference';
import { Box, FormGroup, Card, Button, Typography } from '@material-ui/core';
import Notification from '../universalComponents/Notification';
import Progress from '../universalComponents/Progress';
import { makeStyles } from '@material-ui/core/styles';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';

export default function Form () {
  const [type, setType] = useState('');
  const [code, setCode] = useState('');
  const [name, setName] = useState('');

  const dispatch = useDispatch();

  const useStyles = makeStyles({
    root: {
      marginTop: '30px'
    },
    button: {
      background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
      border: 0,
      borderRadius: 3,
      boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
      color: 'white',
      height: 48,
      padding: '0 30px'
    },
    card: {
      padding: '30px 30px 30px 30px'
    },
    input: {
      width: '100%',
      marginTop: '20px'
    }
  });

  const classes = useStyles();
  const referenceForms = useRef();

  const clearForm = () => {
    referenceForms.current.resetValidations();
    setType('');
    setName('');
    setCode('');
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const create = createReference({ type, code, name });
    dispatch(create);
    dispatch({ type: 'IN_PROGRESS' });
    clearForm();
  };

  const capitalizeAllLetter = (value, callback) => {
    callback(value.slice(0, value.length).toUpperCase());
  };

  return (
    <>
      <Progress />
      <Box display="flex" justifyContent="space-around" className={classes.root}>
        <Card className={classes.card} variant="outlined">
          <Typography variant="h4">Add reference</Typography>
          <ValidatorForm
            ref={referenceForms}
            component="form"
            onSubmit={handleSubmit}
          >
            <FormGroup>
              <TextValidator
                label="Type"
                onChange={(e) => capitalizeAllLetter(e.target.value, setType)}
                value={type}
                validators={['required']}
                errorMessages={['please insert type']}
                className={classes.input}
                helperText={'PRODUCT_TYPE, TRANSACTION_TYPE (auto capital all letter)'}
              >
              </TextValidator>
              <TextValidator
                label="Code"
                onChange={(e) => capitalizeAllLetter(e.target.value, setCode)}
                value={code}
                validators={['required']}
                errorMessages={['please insert code']}
                className={classes.input}
                helperText={'ACC, ADPT, OPR (auto capital all letter)'}
              >
              </TextValidator>
              <TextValidator
                label="Name"
                onChange={(e) => capitalizeAllLetter(e.target.value, setName)}
                value={name}
                validators={['required']}
                errorMessages={['please insert name']}
                className={classes.input}
                helperText={'accessories, operational (auto capital first letter)'}
              >
              </TextValidator>
              <br></br>
              <Button className={classes.button} color="primary" type="submit">Submit</Button>
              <Button variant="contained" onClick={ () => clearForm() } >Reset form</Button>
            </FormGroup>
          </ValidatorForm>
        </Card>
      </Box>
      <Notification/>
    </>
  );
}
