import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { createAbsent } from '../actions/Absent';
import { Box, FormGroup, MenuItem, Card, Button, Typography } from '@material-ui/core';
import Notification from '../universalComponents/Notification';
import Progress from '../universalComponents/Progress';
import { makeStyles } from '@material-ui/core/styles';
import { ValidatorForm, SelectValidator } from 'react-material-ui-form-validator';

export default function Form () {
  const [selectedName, setSelectedName] = useState({
    name: ''
  });

  const dispatch = useDispatch();

  const useStyles = makeStyles({
    root: {
      marginTop: '30px'
    },
    button: {
      background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
      border: 0,
      borderRadius: 3,
      boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
      color: 'white',
      height: 48,
      padding: '0 30px'
    },
    card: {
      padding: '30px 30px 30px 30px'
    },
    select: {
      width: '100%'
    }
  });

  const classes = useStyles();

  const handleSubmit = (e) => {
    e.preventDefault();
    const create = createAbsent(selectedName);
    dispatch(create);
    dispatch({ type: 'IN_PROGRESS' });
    setSelectedName({ name: '' });
  };

  return (
    <>
      <Progress />
      <Box display="flex" justifyContent="space-around" className={classes.root}>
        <Card className={classes.card} variant="outlined">
          <Typography variant="h4">Form absensi harian</Typography>
          <ValidatorForm
            component="form"
            onSubmit={handleSubmit}
          >
            <FormGroup>
              <SelectValidator
                label="Name"
                onChange={(e) => setSelectedName({ name: e.target.value })}
                value={selectedName.name}
                validators={['required']}
                errorMessages={['please select data']}
                className={classes.select}
              >
                <MenuItem value="aldi">aldi</MenuItem>
                <MenuItem value="alam">alam</MenuItem>
                <MenuItem value="ridho">ridho</MenuItem>
                <MenuItem value="rio">rio</MenuItem>
                <MenuItem value="romi">romi</MenuItem>
              </SelectValidator>
              <br></br>
              <Button className={classes.button} color="primary" type="submit">Submit</Button>
            </FormGroup>
          </ValidatorForm>
        </Card>
      </Box>
      <Notification/>
    </>
  );
}
