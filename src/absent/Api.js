import axios from 'axios';

const ABSENT_URL = 'http://localhost:5000/absent';

export const getAbsents = () => axios.get(ABSENT_URL, { timeout: 5000 });
export const createAbsent = (newAbsent) => axios.post(ABSENT_URL, newAbsent, { timeout: 5000 });
