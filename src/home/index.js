import React from 'react';
import { Box, Card } from '@material-ui/core';

export default function () {
  return (
    <div>
      <h2>Home</h2>
      <Box display='flex' justifyContent='space-around'>
        <Card>
          <h2>A</h2>
        </Card>
        <Card>
          <h2>B</h2>
        </Card>
      </Box>
    </div>
  );
}
