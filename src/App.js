/* eslint-disable no-unused-vars */
import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useLocation
} from 'react-router-dom';
import Absent from './absent/';
import Home from './home/';
import ServiceQueue from './serviceQueue';
import ServiceDone from './serviceDone';
import Reference from './reference/';
import Products from './product/';
import clsx from 'clsx';
import {
  Menu, Home as HomeIcon,
  MonetizationOn,
  Settings,
  EmojiPeople,
  SupervisedUserCircle,
  Build,
  VerticalSplit,
  ExpandLess,
  ExpandMore,
  Done,
  Queue
} from '@material-ui/icons';
import { makeStyles } from '@material-ui/core/styles';
import {
  AppBar,
  Toolbar,
  Typography,
  IconButton,
  Drawer,
  ListItemIcon,
  ListItemText,
  List,
  ListItem,
  Collapse
} from '@material-ui/core';

function useQuery () {
  return new URLSearchParams(useLocation().search);
}

function ServiceMenu () {
  const query = useQuery();
  return (
    <>
      {query.get('status') === 'queue'
        ? (<ServiceQueue></ServiceQueue>)
        : (<ServiceDone></ServiceDone>)}
    </>
  );
}

function App () {
  const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1
    },
    menuButton: {
      marginRight: theme.spacing(2)
    },
    title: {
      flexGrow: 1
    },
    list: {
      width: 250
    },
    fullList: {
      width: 'auto'
    },
    customAppBar: {
      background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
      border: 0
    },
    nested: {
      paddingLeft: theme.spacing(4)
    }
  }));
  const classes = useStyles();
  const [state, setState] = React.useState({
    top: false,
    left: false,
    bottom: false,
    right: false
  });

  const [expandServiceMenu, setExpandServiceMenu] = React.useState(false);

  const handleExpandServiceMenu = () => {
    setExpandServiceMenu(!expandServiceMenu);
  };

  const toggleDrawer = (anchor, open) => (event) => {
    if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }

    setState({ ...state, [anchor]: open });
  };

  const list = (anchor) => (
    <div
      className={clsx(classes.list, {
        [classes.fullList]: anchor === 'top' || anchor === 'bottom'
      })}
      role="presentation"
      onKeyDown={toggleDrawer(anchor, false)}
    >
      <List>
        <ListItem button key={'Absent'} component={Link} to="/absent"
          onClick={toggleDrawer(anchor, false)}
        >
          <ListItemIcon><EmojiPeople /></ListItemIcon>
          <ListItemText primary={'Absent'} />
        </ListItem>
        <ListItem button key={'Service'} onClick={() => {
          handleExpandServiceMenu();
          toggleDrawer(anchor, true);
        }}>
          <ListItemIcon><Build /></ListItemIcon>
          <ListItemText primary={'Service'} />
          { expandServiceMenu ? <ExpandLess /> : <ExpandMore />}
        </ListItem>
        <Collapse in={expandServiceMenu} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            <ListItem button className={classes.nested} component={Link} to="/services?status=queue"
              onClick={toggleDrawer(anchor, false)}
            >
              <ListItemIcon>
                <Queue />
              </ListItemIcon>
              <ListItemText primary="Queue" />
            </ListItem>
            <ListItem button className={classes.nested} component={Link} to="/services?status=done"
              onClick={toggleDrawer(anchor, false)}
            >
              <ListItemIcon>
                <Done />
              </ListItemIcon>
              <ListItemText primary="Done" />
            </ListItem>
          </List>
        </Collapse>
        <ListItem button key={'Reference'} component={Link} to="/reference"
          onClick={toggleDrawer(anchor, false)}
        >
          <ListItemIcon><Settings /></ListItemIcon>
          <ListItemText primary={'Reference'} />
        </ListItem>
        <ListItem button key={'Products'} component={Link} to="/product"
          onClick={toggleDrawer(anchor, false)}
        >
          <ListItemIcon><VerticalSplit /></ListItemIcon>
          <ListItemText primary={'Products'} />
        </ListItem>
        <ListItem button key={'Monetary'} component={Link} to="/stock"
          onClick={toggleDrawer(anchor, false)}
        >
          <ListItemIcon><MonetizationOn /></ListItemIcon>
          <ListItemText primary={'Monetary'} />
        </ListItem>
        <ListItem button key={'Clients'} component={Link} to="/stocks"
          onClick={toggleDrawer(anchor, false)}
        >
          <ListItemIcon><SupervisedUserCircle /></ListItemIcon>
          <ListItemText primary={'Clients'} />
        </ListItem>
        <ListItem button key={'Home'} component={Link} to="/"
          onClick={toggleDrawer(anchor, false)}
        >
          <ListItemIcon><HomeIcon /></ListItemIcon>
          <ListItemText primary={'Home'} />
        </ListItem>
      </List>
    </div>
  );

  const anchor = 'left';

  return (
    <Router>
      <AppBar position="static" className={classes.customAppBar}>
        <Toolbar>
          <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu" onClick={toggleDrawer(anchor, true)}>
            <Menu />
          </IconButton>
          <Typography variant="h6" className={classes.title}>
              Navigation
          </Typography>
        </Toolbar>
      </AppBar>
      <Drawer anchor={anchor} open={state[anchor]} onClose={toggleDrawer(anchor, false)}>
        {list(anchor)}
      </Drawer>
      <Switch>
        <Route path="/absent">
          <Absent></Absent>
        </Route>
        <Route path="/reference">
          <Reference></Reference>
        </Route>
        <Route path="/product">
          <Products></Products>
        </Route>
        <Route path="/services">
          <ServiceMenu></ServiceMenu>
        </Route>
        <Route path="/">
          <Home></Home>
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
